function [R_1, R_2] = partition(Y, p, r, d)
    % Construct the lags and threshold variable
    data = lagmatrix(Y,[0:p d]);

    % Remove the first max(p,d) observations
    data = data(max(p,d)+1:end,:);

    % Add the regime
    data = [data (data(:,end) > r)+1];

    % Partition into regime-specific matrices
    % (and remove the the two columns we just created)
    R_1 = data(data(:,end)==1,1:end-2); % lower regime (y_{t-d} <= r)
    R_2 = data(data(:,end)==2,1:end-2); % upper regime (y_{t-d} > r)