% General
clear;
clc;

% General parameters
N = 100;
T = 200;
G0 = 2200;
G = 1000;

% seed the random number generation for repeatable results
rng(12345);

% store all results
draws = zeros((G0+G)*N,7);
accept = zeros(N,1);
time = zeros(N,1);

for i = 1:N,
    % Simulate data
    Y = simulate(T,1);

    tic;
    [accept(i), theta_1_mm, theta_2_mm, sigma2_1_mm, sigma2_2_mm, d_mm, r_mm] = estimate(Y, G0, G);
    time(i) = toc;
    
    rows = (G0+G)*(i-1)+1:(G0+G)*i;
    run_draws = [               ...
        i*ones(G0+G,1)          ...
        theta_1_mm              ...
        theta_2_mm              ...
        sigma2_1_mm             ...
        sigma2_2_mm             ...
        d_mm                    ...
        r_mm                    ...
    ];

    draws(rows,:) = run_draws;
end;

csvwrite('draws.csv',draws);
csvwrite('models.csv',[(1:N)' accept time]);