function draw = sample_d(d0, Y, p, theta_1, theta_2, sigma2_1, sigma2_2, r)
    prob = rand();
    lls = zeros(d0,1);
    scale = zeros(d0,1);
    mantissa = zeros(d0,1);
    exponent = zeros(d0,1);
    
    for d = 1:d0,
        % the likelihood is given by (mantissa^exponent)*scale
        [scale(d), mantissa(d), exponent(d), discard] = L(Y, p, theta_1, theta_2, sigma2_1, sigma2_2, r, d);
    end;
    % rescale all the scales
    scale = scale / max(scale);
    % rescale all the exponents
    exponent = exponent - mean(exponent);
    % now construct the likelihoods, without precision problems
    for d = 1:d0,
        lls(d) = (mantissa(d)*10^exponent(d))/scale(d);
    end;
    
    denominator = sum(lls);
    
    cumulative = 0;
    for d = 1:d0,
        cumulative = cumulative + lls(d)/denominator;
        if cumulative > prob
            break
        end
    end;
    
    draw = d;