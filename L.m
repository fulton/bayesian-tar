function [scale, mantissa, exponent, l] = L(Y, p, theta_1, theta_2, sigma2_1, sigma2_2, r, d)
    [R1, R2] = partition(Y,p,r,d);
    
    Y1 = R1(:,1);
    Y2 = R2(:,1);
    X1 = R1(:,2:end);
    X2 = R2(:,2:end);
    
    T1 = length(Y1);
    T2 = length(Y2);
    
    resid1 = (Y1 - X1*theta_1);
    resid2 = (Y2 - X2*theta_2);
    
    l_1 = (1/(2*sigma2_1)) * (resid1'*resid1);
    l_2 = (1/(2*sigma2_2)) * (resid2'*resid2);
    
    scale = ((sigma2_1^(T1/2)) * (sigma2_2^(T2/2)));
    
    % The number will be too small
    a = -(l_1 + l_2);
    x = a/log(10);
    exponent = floor(x);
    mantissa = 10^(x-exponent);
    
    l = (mantissa*10^exponent)/scale;