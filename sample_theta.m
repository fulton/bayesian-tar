function draw = sample_theta(theta, V, sigma2, R)
    Y = R(:,1);
    X = R(:,2:end);
    XX = X'*X;
    Vbar = inv(XX/sigma2 + V);
    
    theta_hat = inv(XX)*(X'*Y);
    thetabar = Vbar*((XX/sigma2)*theta_hat + V*theta);
    
    draw = mvnrnd(thetabar, Vbar, 1);