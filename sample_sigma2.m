function draw = sample_sigma2(v, lambda, theta, R)
    Y = R(:,1);
    X = R(:,2:end);
    T = length(Y);
    resid = (Y - X*theta);
    
    s2 = (resid'*resid) / T;
    
    a = (v + T)/2;
    b = (v*lambda + s2*T)/2;

    draw = 1./gamrnd(a,1./b);