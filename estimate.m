function [accept, theta_1_mm, theta_2_mm, sigma2_1_mm, sigma2_2_mm, d_mm, r_mm] = estimate(Y, G0, G)

% General parameters
T = length(Y);
p_1 = 1;
p_2 = 1;
p = max(p_1,p_2);

% - Fit an AR(2) ---------------------------------------------------------%
% This is used for setting the lambda hyperparameters below
data = [ones(length(Y),1) lagmatrix(Y,1:2)];
X = data(3:end,2:end);
ar_coeffs = inv(X'*X)*X'*Y(3:end);
Yhat = X*ar_coeffs;
resid = Y(3:end)-Yhat;
mse = sum(resid.^2)/length(resid);

% - Hyperparameters ------------------------------------------------------%

% Hyperparamters on \Theta_1 and \Theta_2 (MVN)
theta_1_prior = zeros(p_1, 1); % location of MVN
theta_2_prior = zeros(p_2, 1); % location of MVN

h = 0.1;
V_1 = h * eye(p_1);         % covariance of MVN
V_2 = h * eye(p_2);         % covariance of MVN

% Hyperparameters on \sigma_1^2 and \sigma_2^2 (IG)
v_1 = 3;         % IG shape parameter
v_2 = 3;         % IG shape parameter
lambda_1 = mse/3;
lambda_2 = mse/3;

% Other
d0 = 4;                  % maximum d to consider, as in Chen and Lee

% - Initial values -------------------------------------------------------%
% (start around the true values, somewhere)
sigma2_1 = 0.5;
sigma2_2 = 0.5;
r = 0;
d = 2;

% - Metropolis-Hastings Parameters ---------------------------------------%
% Parameters on r (uniform)
r_space = prctile(Y, [10,90]);
a = r_space(1);          % lower bound for uniform
b = r_space(2);          % upper bound for uniform
tau = 0.1;               % variance parameter for random walk proposal

% - Sampling Parameters --------------------------------------------------%
%G0 = 20000;             % Burn-in draws
%G = 10000;              % Post-convergence draws

% Metropolis-Hastings Diagnostics
accept = 0;

% Storage of draws
theta_1_mm = zeros(G0+G, p_1);
theta_2_mm = zeros(G0+G, p_2);
sigma2_1_mm = zeros(G0+G, 1);
sigma2_2_mm = zeros(G0+G, 1);
d_mm = zeros(G0+G, 1);
r_mm = zeros(G0+G, 1);

% - Sampling -------------------------------------------------------------%

for i = 1:G0+G,
    
    % Get our partitions for the d and r we have
    [R1, R2] = partition(Y,p,r,d);
    
    % Draw a value of theta1
    theta_1 = sample_theta(theta_1_prior, V_1, sigma2_1, R1);
    
    % Draw a value of theta2
    theta_2 = sample_theta(theta_2_prior, V_2, sigma2_2, R2);
    
    % Draw a value of sigma2_1
    sigma2_1 = sample_sigma2(v_1, lambda_1, theta_1, R1);
    % Draw a value of sigma2_2
    sigma2_2 = sample_sigma2(v_2, lambda_2, theta_2, R2);
    
    % Draw a value of d
    d = sample_d(d0, Y, p, theta_1, theta_2, sigma2_1, sigma2_2, r);
    
    % Metropolis-Hastings to get a draw of r
    % transform r to be in (-infty,infty)
    rt = log((r - a)/(b - r));
    % get (transformed) proposal according to a random walk proposal
    % (so that q is N(r,tau))
    rt_star = (rt + randn(1)*(tau^0.5));
    % untransform the proposal
    r_star = (a + exp(rt_star)*b) / (1 + exp(rt_star));
    
    % calculate likelihoods (using untransformed parameters)
    lls = zeros(2,1);
    r_scale = zeros(2,1);
    r_mantissa = zeros(2,1);
    r_exponent = zeros(2,1);
    [r_scale(1), r_mantissa(1), r_exponent(1), discard] = L(Y, p, theta_1, theta_2, sigma2_1, sigma2_2, r, d);
    [r_scale(2), r_mantissa(2), r_exponent(2), discard] = L(Y, p, theta_1, theta_2, sigma2_1, sigma2_2, r_star, d);
    
    % Now construct the acceptance probability
    acceptance_prob = min(1, ( (r_mantissa(2)/r_mantissa(1))*(10^(r_exponent(2)-r_exponent(1)))*(r_scale(1)/r_scale(2)) ));
    prob = rand();
    if prob <= acceptance_prob,
        r = r_star;
        accept = accept + 1;
    end
       
    % Record data
    if i >= 0,
        theta_1_mm(i,:) = theta_1;
        theta_2_mm(i,:) = theta_2;
        sigma2_1_mm(i,:) = sigma2_1;
        sigma2_2_mm(i,:) = sigma2_2;
        d_mm(i,:) = d;
        r_mm(i,:) = r;
    end;
    
end;