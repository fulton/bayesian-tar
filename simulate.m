function Y = simulate(T,d)
    % Simulates a TAR(2;r, 1) model    
    
    % @todo make these arguments
    phi_1 = -0.5;
    phi_2 = 0.5;
    sigma2_1 = 2.0;
    sigma2_2 = 1.0;
    r = 0.40;
    p = 1; % max(p_1,p_2)
    n = max(p,d); % number of initial observations we need to condition on
    
    Y = zeros(T+n,1); % add n so that when we cutoff the first n values
                        % we're left with T observations
    regime = zeros(T+n,1);
    for i = (n+1):T+n,
        if Y(i-d) <= r
            regime(i) = 1;
            Y(i) = phi_1*Y(i-d) + randn(1)*(sigma2_1^0.5);
        else
            regime(i) = 2;
            Y(i) = phi_2*Y(i-d) + randn(1)*(sigma2_2^0.5);
        end
    end;
    %[lagmatrix(Y,[0,d]) r*ones(T+n,1) regime]; %diagnostics